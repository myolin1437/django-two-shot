from django.urls import path


from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    CategoryListView,
    AccountListView,
    CategoryCreateView,
    AccountCreateView,
)


# from receipts.views import (
#     receipts_list,
#     create_receipt,
#     categories_list,
#     accounts_list,
#     create_category,
#     create_account,
# )


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", CategoryListView.as_view(), name="categories_list"),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path(
        "categories/create/",
        CategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]


# urlpatterns = [
#     path("", receipts_list, name="home"),
#     path("create/", create_receipt, name="create_receipt"),
#     path("categories/", categories_list, name="categories_list"),
#     path("accounts/", accounts_list, name="accounts_list"),
#     path("categories/create/", create_category, name="create_category"),
#     path("accounts/create/", create_account, name="create_account"),
# ]

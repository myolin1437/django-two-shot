from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from receipts.models import (
    Receipt,
    ExpenseCategory,
    Account,
)

from receipts.forms import (
    ReceiptForm,
    CategoryForm,
    AccountForm,
)


# Create your views here.
#################################### RECEIPT LIST VIEW ####################################
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


# @login_required(login_url="/accounts/login/")
# def receipts_list(request):
#     user = request.user
#     receipt_list = Receipt.objects.filter(purchaser=user)
#     context = {
#         "receipt_list": receipt_list,
#     }

#     return render(request, "receipts/list.html", context)


#################################### RECEIPT CREATE VIEW ####################################
class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


# @login_required(login_url="/accounts/login/")
# def create_receipt(request):
#     form = ReceiptForm()
#     if request.method == "POST":
#         form = ReceiptForm(request.POST)
#         receipt = form.save(commit=False)
#         receipt.purchaser = request.user
#         receipt.save()
#         return redirect("home")
#     context = {
#         "form": form,
#     }

#     return render(request, "receipts/create.html", context)


#################################### CATEGORY LIST VIEW ####################################
class CategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


# @login_required(login_url="/accounts/login/")
# def categories_list(request):
#     user = request.user
#     expensecategory_list = ExpenseCategory.objects.filter(owner=user)
#     context = {
#         "expensecategory_list": expensecategory_list,
#     }

#     return render(request, "categories/list.html", context)


#################################### CATEGORY CREATE VIEW ####################################
class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "categories/create.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("categories_list")


# @login_required(login_url="/accounts/login/")
# def create_category(request):
#     form = CategoryForm()
#     if request.method == "POST":
#         form = CategoryForm(request.POST)
#         category = form.save(commit=False)
#         category.owner = request.user
#         category.save()
#         return redirect("categories_list")
#     context = {
#         "form": form,
#     }

#     return render(request, "categories/create.html", context)


#################################### ACCOUNT LIST VIEW ####################################
class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


# @login_required(login_url="/accounts/login/")
# def accounts_list(request):
#     user = request.user
#     account_list = Account.objects.filter(owner=user)
#     context = {
#         "account_list": account_list,
#     }

#     return render(request, "accounts/list.html", context)


#################################### ACCOUNT CREATE VIEW ####################################
class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("accounts_list")


# @login_required(login_url="/accounts/login/")
# def create_account(request):
#     form = AccountForm()
#     if request.method == "POST":
#         form = AccountForm(request.POST)
#         category = form.save(commit=False)
#         category.owner = request.user
#         category.save()
#         return redirect("accounts_list")
#     context = {
#         "form": form,
#     }

#     return render(request, "accounts/create.html", context)

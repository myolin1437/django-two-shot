from django.contrib import admin

from receipts.models import (
    ExpenseCategory,
    Account,
    Receipt,
)


# Register your models here.
class AdminExpenseCategory(admin.ModelAdmin):
    pass


class AdminAccount(admin.ModelAdmin):
    pass


class AdminReceipt(admin.ModelAdmin):
    pass


admin.site.register(ExpenseCategory, AdminExpenseCategory)
admin.site.register(Account, AdminAccount)
admin.site.register(Receipt, AdminReceipt)
